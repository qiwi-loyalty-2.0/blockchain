extern crate exonum;
extern crate exonum_testkit;
extern crate qiwiloyalty;

use exonum::crypto::{self, PublicKey, SecretKey};
use qiwiloyalty::{
    QiwiloyaltyService,
    schema::{
        User,
        Store,
        ConversionList,
        Coefficients,
    },
    transactions::{
        TxCreateUser,
        TxCreateStore,
        TxAddBonus,
        TxTakeBonus,
        TxAddPermissionToProducingBonuses,
        TxRemovePermissionToProducingBonuses,
        TxAddConversion,
        TxTakeBonusWithConversion
    },
    api::{
        TransactionResponse,
        UserQuery,
        StoreQuery
    }
};

use exonum_testkit::{TestKitApi, TestKitBuilder, ApiKind};

trait MyServiceApi {
    fn get_store(&self, id : String) -> Store;
    fn get_stores(&self) -> Vec<Store>;
    fn create_store(&self, owner : &PublicKey, name : &str, secret_key: &SecretKey) -> TransactionResponse;

    fn get_user(&self, pub_key : PublicKey) -> User;
    fn get_users(&self) -> Vec<User>;
    fn create_user(&self) -> (TransactionResponse, PublicKey, SecretKey);

    fn add_bonus(&self, from: PublicKey, pub_key : PublicKey, name: String, amount: u64,
                 combustion : u64, priv_key: &SecretKey) -> TransactionResponse;
    fn take_bonus(&self, who_takes: &PublicKey, pub_key : PublicKey, name: String,
                  amount: u64, seed: u64, priv_key: &SecretKey)
        -> TransactionResponse;

    fn add_permission(&self, who_gives: &PublicKey, who_gets: &PublicKey, store_name: &str,
                      private_key: &SecretKey) -> TransactionResponse;

    fn remove_permission(&self, who_removes: &PublicKey, who_loses: &PublicKey, store_name: &str,
                        private_key: &SecretKey) -> TransactionResponse;

    fn get_conversion(&self, id: String) -> ConversionList;

    fn add_conversion(&self, first_side_owner: &PublicKey, first_side: &str, second_side: &str,
                      coefficients: Coefficients, private_key: &SecretKey) -> TransactionResponse;

    fn take_bonus_with_conversion(&self, who_takes: &PublicKey,
                                  who_pays: &PublicKey,
                                  store_name: &str,
                                  paying_store_name: &str,
                                  amount: u64,
                                  seed: u64, private_key: &SecretKey) -> TransactionResponse;
}

impl MyServiceApi for TestKitApi {
    fn get_store(&self, name: String) -> Store {
        let sq = StoreQuery{name};
        self.public(ApiKind::Service("qiwiloyalty"))
            .query(&sq)
            .get(&format!("v1/store"))
            .unwrap()
    }

    fn get_stores(&self) -> Vec<Store> {
        self.public(ApiKind::Service("qiwiloyalty"))
            .get(&format!("v1/stores"))
            .unwrap()
    }

    fn create_store(&self, owner: &PublicKey, name: &str, secret_key: &SecretKey) -> TransactionResponse {
        let tx = TxCreateStore::new(owner, name, secret_key);

        let resp : TransactionResponse = self.public(ApiKind::Service("qiwiloyalty"))
             .query(&tx)
             .post("v1/stores")
             .unwrap();

        resp
    }

    fn get_user(&self, pub_key: PublicKey) -> User {
        let uq = UserQuery{pub_key};
        self.public(ApiKind::Service("qiwiloyalty"))
            .query(&uq)
            .get(&format!("v1/user"))
            .unwrap()
    }

    fn get_users(&self) -> Vec<User> {
        self.public(ApiKind::Service("qiwiloyalty"))
            .get(&format!("v1/users"))
            .unwrap()
    }

    fn create_user(&self) -> (TransactionResponse, PublicKey, SecretKey) {
        let (pubkey, key) = crypto::gen_keypair();

        let tx = TxCreateUser::new(&pubkey, &key);

        let resp : TransactionResponse = self.public(ApiKind::Service("qiwiloyalty"))
             .query(&tx)
             .post("v1/users")
             .unwrap();

        (resp, pubkey, key)
    }

    fn add_bonus(&self, from: PublicKey, pub_key : PublicKey, name: String, amount: u64,
                 combustion : u64, priv_key: &SecretKey) -> TransactionResponse {

        let tx = TxAddBonus::new(&from, &pub_key, amount, &name, combustion, priv_key);
        let resp = self.public(ApiKind::Service("qiwiloyalty"))
                    .query(&tx)
                    .post("v1/users/addbonus")
                    .unwrap();
        resp
    }

    fn take_bonus(&self, who_takes: &PublicKey, pub_key : PublicKey, name: String,
                  amount: u64, seed: u64, priv_key: &SecretKey) -> TransactionResponse {
        let tx = TxTakeBonus::new(who_takes, &pub_key, &name, amount, seed, priv_key);
        let resp = self.public(ApiKind::Service("qiwiloyalty"))
                    .query(&tx)
                    .post("v1/users/takebonus")
                    .unwrap();
        resp
    }

    fn add_permission(&self, who_gives: &PublicKey, who_gets: &PublicKey, store_name: &str,
                      private_key: &SecretKey) -> TransactionResponse {
        let tx = TxAddPermissionToProducingBonuses::new(
            who_gives,
            who_gets,
            store_name,
            &private_key);

        let resp = self.public(ApiKind::Service("qiwiloyalty"))
            .query(&tx)
            .post("v1/users/addpermission")
            .unwrap();

        resp
    }

    fn remove_permission(&self, who_removes: &PublicKey, who_loses: &PublicKey, store_name: &str,
                         private_key: &SecretKey) -> TransactionResponse {
        let tx = TxRemovePermissionToProducingBonuses::new(
            who_removes,
            who_loses,
            store_name,
            &private_key
        );

        self.public(ApiKind::Service("qiwiloyalty"))
            .query(&tx)
            .post("v1/users/removepermission")
            .unwrap()
    }

    fn get_conversion(&self, id: String) -> ConversionList {
        let sq = StoreQuery{name: id};
        self.public(ApiKind::Service("qiwiloyalty"))
            .query(&sq)
            .get(&format!("v1/conversions"))
            .unwrap()
    }

    fn add_conversion(&self, first_side_owner: &PublicKey, first_side: &str, second_side: &str,
                      coefficients: Coefficients, private_key: &SecretKey) -> TransactionResponse
    {
        let tx = TxAddConversion::new(
            first_side_owner,
            first_side,
            second_side,
            coefficients,
            &private_key
        );

        self.public(ApiKind::Service("qiwiloyalty"))
            .query(&tx)
            .post("v1/conversions/addconversion")
            .unwrap()
    }

    fn take_bonus_with_conversion(&self, who_takes: &PublicKey, who_pays: &PublicKey,
                                  store_name: &str, paying_store_name: &str,
                                  amount: u64, seed: u64,
                                  private_key: &SecretKey) -> TransactionResponse
    {
        let tx = TxTakeBonusWithConversion::new(
            who_takes,
            who_pays,
            store_name,
            paying_store_name,
            amount,
            seed,
            &private_key
        );

        self.public(ApiKind::Service("qiwiloyalty"))
            .query(&tx)
            .post("v1/users/takebonuswithconversion")
            .unwrap()
    }
}

#[test]
fn my_api_test() {
    let mut testkit = TestKitBuilder::validator()
        .with_service(QiwiloyaltyService)
        .create();

    // Check API responses
    let api = testkit.api();
    
    let (_, pub_key, sec_key) = api.create_user();
    testkit.create_block();
    
    let users = api.get_users();
    assert_eq!(users.len(), 1);
    assert_eq!(*users[0].pub_key(), pub_key);
    testkit.create_block();

    let user = api.get_user(pub_key);
    assert_eq!(*user.pub_key(), pub_key);
    testkit.create_block();

    let _ = api.create_store(&pub_key, "my_store", &sec_key);
    testkit.create_block();

    let store = api.get_store("my_store".to_owned());
    assert_eq!(store.name(), "my_store");

    let (_, sender_pk, sender_sk) = api.create_user();
    testkit.create_block();

    api.add_permission(&pub_key, &sender_pk, "my_store",
                       &sec_key);

    testkit.create_block();

    api.add_bonus(sender_pk, pub_key, "my_store".to_owned(), 12, 12,
                  &sender_sk);

    testkit.create_block();
    let user = api.get_user(pub_key);
    assert_eq!(user.bonuses().len(), 1);

    api.take_bonus(&sender_pk, pub_key, "my_store".to_owned(), 11, 10,
                   &sender_sk);
    testkit.create_block();
    let user = api.get_user(pub_key);
    assert_eq!(user.bonuses()[0].amount(), 1);
}

#[test]
fn add_permission_end_point() {
    let mut testkit = TestKitBuilder::validator()
        .with_service(QiwiloyaltyService)
        .create();

    // Check API responses
    let api = testkit.api();

    let (_, pub_key, _sec_key) = api.create_user();
    let (_, store_owner_pub_key, store_owner_secret_key) = api.create_user();

    testkit.create_block();

    api.create_store(&store_owner_pub_key, "magnit",
                     &store_owner_secret_key);

    testkit.create_block();

    api.add_permission(&store_owner_pub_key, &pub_key, "magnit",
                       &store_owner_secret_key);

    let tx_info = testkit.create_block();

    for tx in tx_info.iter() {
        assert!(tx.status().is_ok());
    }

    let user = api.get_user(pub_key);
    assert!(user.contains_bonus_for_producing("magnit"));
}

#[test]
fn remove_permission_end_point() {
    let mut testkit = TestKitBuilder::validator()
        .with_service(QiwiloyaltyService)
        .create();

    // Check API responses
    let api = testkit.api();

    let (_, pub_key, _sec_key) = api.create_user();
    let (_, store_owner_pub_key, store_owner_secret_key) = api.create_user();

    testkit.create_block();

    api.create_store(&store_owner_pub_key, "magnit",
                     &store_owner_secret_key);

    testkit.create_block();

    api.add_permission(&store_owner_pub_key, &pub_key, "magnit",
                       &store_owner_secret_key);

    testkit.create_block();

    api.remove_permission(&store_owner_pub_key, &pub_key, "magnit",
                          &store_owner_secret_key);

    let tx_info = testkit.create_block();

    for tx in tx_info.iter() {
        assert!(tx.status().is_ok());
    }

    let user = api.get_user(pub_key);
    assert!(!user.contains_bonus_for_producing("magnit"));
}

#[test]
fn test_api_add_conversion() {
    let mut testkit = TestKitBuilder::validator()
        .with_service(QiwiloyaltyService)
        .create();

    let api = testkit.api();

    let (_, pubkey1, sec1) = api.create_user();
    let (_, pubkey2, sec2) = api.create_user();

    testkit.create_block();

    let store1 = "A".to_owned();
    let store2 = "B".to_owned();

    api.create_store(&pubkey1, &store1, &sec1);
    api.create_store(&pubkey2, &store2, &sec2);

    testkit.create_block();

    api.add_conversion(&pubkey1, &store1, &store2,
        Coefficients::new(1, 2), &sec1);
    api.add_conversion(&pubkey2, &store2, &store1,
                       Coefficients::new(2, 1), &sec2);

    let tx_info = testkit.create_block();

    for tx in tx_info.iter() {
        assert!(tx.status().is_ok());
    }

    let list = api.get_conversion(store1.clone());
    assert!(list.get_by_second_side(&store2).is_some());
    let list = api.get_conversion(store2.clone());
    assert!(list.get_by_second_side(&store1).is_some());
}

#[test]
fn api_test_take_bonus_with_conversion() {
    let mut testkit = TestKitBuilder::validator()
        .with_service(QiwiloyaltyService)
        .create();

    let api = testkit.api();

    let (_, pubkey1, sec1) = api.create_user();
    let (_, pubkey2, sec2) = api.create_user();

    testkit.create_block();

    let store1 = "A".to_owned();
    let store2 = "B".to_owned();

    api.create_store(&pubkey1, &store1, &sec1);
    api.create_store(&pubkey2, &store2, &sec2);

    testkit.create_block();

    api.add_conversion(&pubkey1, &store1, &store2,
                       Coefficients::new(1, 2), &sec1);
    api.add_conversion(&pubkey2, &store2, &store1,
                       Coefficients::new(2, 1), &sec2);

    testkit.create_block();

    api.add_permission(&pubkey1, &pubkey1, &store1, &sec1);
    api.add_permission(&pubkey2, &pubkey2, &store2, &sec2);

    testkit.create_block();

    api.add_bonus(pubkey1, pubkey1, store1.clone(), 10, 100, &sec1);

    testkit.create_block();

    api.take_bonus_with_conversion(&pubkey2, &pubkey1,
                                   &store2, &store1,
                                   18, 100, &sec2);

    let tx_info = testkit.create_block();

    for tx in tx_info.iter() {
        assert!(tx.status().is_ok());
    }

    assert_eq!(1, api.get_user(pubkey1).bonuses()[0].amount());
}
