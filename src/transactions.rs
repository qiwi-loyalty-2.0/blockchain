use exonum::{
    blockchain::{ExecutionError,
                 ExecutionResult, Transaction
    },
    crypto::PublicKey,
    messages::Message,
    storage::Fork
};

use crate::SERVICE_ID;
use crate::schema::{Store, QiwiloyaltySchema, Bonus, User, Coefficients, Conversion, ConversionList};

transactions! {
    // Transaction group.
    pub QiwiLoyaltyTransactions {
        const SERVICE_ID = SERVICE_ID;

        // Transaction type for creating a new user.
        struct TxCreateUser {
            pub_key: &PublicKey,
        }

        // Transaction type for adding bonus to wallet
        struct TxAddBonus {
            from: &PublicKey,
            to: &PublicKey,
            amount: u64,
            name: &str,
            combustion: u64,
        }

        // Transaction type for taking bonus from user
        struct TxTakeBonus {
            who_takes: &PublicKey,
            who : &PublicKey,
            name : &str,
            amount : u64,
            seed: u64,
        }

        //Transaction type for creating Store
        struct TxCreateStore {
            owner : &PublicKey,
            name : &str,
        }

        /// Transaction for adding permission to producing bonuses
        struct TxAddPermissionToProducingBonuses {
            who_gives: &PublicKey,
            who_gets: &PublicKey,
            store_name: &str,
        }

        /// Transaction for removing permission to producing bonuses
        struct TxRemovePermissionToProducingBonuses {
            who_removes: &PublicKey,
            who_loses: &PublicKey,
            store_name: &str,
        }

        /// Transaction for adding conversion
        struct TxAddConversion {
            first_side_owner: &PublicKey,
            first_side: &str,
            second_side: &str,
            coefficient: Coefficients,
        }

        struct TxTakeBonusWithConversion {
            who_takes: &PublicKey,
            who_pays: &PublicKey,
            store_name: &str,
            paying_store_name: &str,
            amount: u64,
            seed: u64,
        }
    }
}

#[derive(Debug, Fail)]
#[repr(u8)]
pub enum Error {
    #[fail(display = "User already exists")]
    UserAlreadyExists = 0,

    #[fail(display = "Sender doesn't exist")]
    SenderNotFound = 1,

    #[fail(display = "Receiver doesn't exist")]
    ReceiverNotFound = 2,

    #[fail(display = "Insufficient currency amount")]
    InsufficientBonusesAmount = 3,

    #[fail(display = "User doesn't exist")]
    UserNotExist = 4,

    #[fail(display = "Store already exists")]
    StoreAlreadyExists = 5,

    #[fail(display = "User has no enough bonuses")]
    UserHasNoEnoughBonuses = 6,

    #[fail(display = "User cannot produce bonuses of requested type")]
    UserCannotProduceBonus = 7,

    #[fail(display = "User cannot give or remove permissions to producing this bonus type")]
    UserCannotManagePermission = 8,

    #[fail(display = "Store does not exists")]
    StoreDoesNotExist = 9,

    #[fail(display = "User already has permission to producing this type of bonuses")]
    UserAlreadyHasPermission = 10,

    #[fail(display = "Denominator cannot be 0")]
    ZeroDenominator = 11,

    #[fail(display = "Conversions cannot be submitted")]
    ConversionsDontEquals = 12,

    #[fail(display = "This type of bonuses cannot be used by paying in this case")]
    CannotPayThisTypeOfBonuses = 13,
}

// Conversion between service-specific errors and the standard error type
// that can be emitted by transactions.
impl From<Error> for ExecutionError {
    fn from(value: Error) -> ExecutionError {
        let description = format!("{}", value);
        ExecutionError::with_description(value as u8, description)
    }
}

impl Transaction for TxTakeBonus {
    fn verify(&self) -> bool {
        self.verify_signature(self.who_takes())
    }

    fn execute(&self, view: &mut Fork) -> ExecutionResult {
        let mut schema = QiwiloyaltySchema::new(view);

        let tx_manager = match schema.user(self.who_takes()) {
            Some(val) => val,
            None => Err(Error::SenderNotFound)?,
        };

        if !tx_manager.contains_bonus_for_producing(self.name()) {
            Err(Error::UserCannotProduceBonus)?
        }

        let user = match schema.user(self.who()) {
            Some(val) => val,
            None => Err(Error::UserNotExist)?,
        };

        let (user, result) =  user.take_bonus(&self.name().to_owned(),
                                              self.amount());

        if result {
            let mut users = schema.users_mut();
            users.put(self.who(), user);
            warn!("Took {} {} bonuses from {:?}", self.amount(), self.name(), self.who());
            Ok(())
        } else {
            Err(Error::UserHasNoEnoughBonuses)?
        }
    }
}

impl Transaction for TxCreateStore {
    fn verify(&self) -> bool {
        true
    }

    fn execute(&self, view: &mut Fork) -> ExecutionResult {
        let mut schema = QiwiloyaltySchema::new(view);
        if schema.user(self.owner()).is_none() {
            Err(Error::UserNotExist)?
        } else {
            let name = self.name().to_owned();

            if schema.store(&name).is_none() {
                let store = Store::new(self.owner(), self.name(), false);
                warn!("Create store: success (name: {:?}, owner: {:?}", self.name(), self.owner());
                schema.stores_mut().put(&name, store);
                Ok(())
            } else {
                return Err(Error::StoreAlreadyExists)?
            }
        }
    }
}

impl Transaction for TxAddBonus {
    fn verify(&self) -> bool {
        self.verify_signature(self.from())
    }

    fn execute(&self, view: &mut Fork) -> ExecutionResult {

        let mut schema = QiwiloyaltySchema::new(view);

        let sender = match schema.user(self.from()) {
            Some(val) => val,
            None => Err(Error::SenderNotFound)?,
        };

        if !sender.contains_bonus_for_producing(self.name()) {
            Err(Error::UserCannotProduceBonus)?
        }

        let receiver = match schema.user(self.to()) {
            Some(val) => val,
            None => Err(Error::ReceiverNotFound)?,
        };

        let bonus = Bonus::new(self.to(), self.amount(), self.name(), self.combustion());
        warn!("Add bonus: {:?} to user: {:?}", bonus, receiver);
        let receiver = receiver.add_bonus(bonus);
        let mut wallets = schema.users_mut();
        wallets.put(self.to(), receiver);
        Ok(())
    }
}

impl Transaction for TxCreateUser {
    fn verify(&self) -> bool {
        self.verify_signature(self.pub_key())
    }

    fn execute(&self, view: &mut Fork) -> ExecutionResult {
        let mut schema = QiwiloyaltySchema::new(view);
        if schema.user(self.pub_key()).is_none() {
            let user = User::new(self.pub_key(), vec![], vec![]);
            warn!("Create the user: {:?}", user);
            schema.users_mut().put(self.pub_key(), user);
            Ok(())
        } else {
            Err(Error::UserAlreadyExists)?
        }
    }
}

impl Transaction for TxAddPermissionToProducingBonuses {
    fn verify(&self) -> bool {
        self.verify_signature(self.who_gives())
    }

    fn execute(&self, fork: &mut Fork) -> Result<(), ExecutionError> {
        let mut schema = QiwiloyaltySchema::new(fork);

        if schema.user(self.who_gives()).is_none() {
            Err(Error::UserNotExist)?
        }

        let who_gets_bonuses = match schema.user(self.who_gets()) {
            None => Err(Error::ReceiverNotFound)?,
            Some(x) => x
        };

        let store = match schema.store(&self.store_name().to_owned()) {
            Some(x) => x,
            None => Err(Error::StoreDoesNotExist)?
        };

        if store.owner() != self.who_gives() {
            Err(Error::UserCannotManagePermission)?
        }

        if who_gets_bonuses.contains_bonus_for_producing(self.store_name()) {
            Err(Error::UserAlreadyHasPermission)?
        }

        let new_user = who_gets_bonuses
            .add_permission_to_producing_bonuses(&self.store_name().to_owned());
        schema.users_mut().put(self.who_gets(), new_user);

        warn!("Add permission to adding bonuses {} to {:?}", self.store_name(), self.who_gets());

        Ok(())
    }
}

impl Transaction for TxRemovePermissionToProducingBonuses {
    fn verify(&self) -> bool {
        self.verify_signature(self.who_removes())
    }

    fn execute(&self, fork: &mut Fork) -> Result<(), ExecutionError> {
        let mut schema = QiwiloyaltySchema::new(fork);

        if schema.user(self.who_removes()).is_none() {
            Err(Error::UserNotExist)?
        }

        let who_loses_bonuses = match schema.user(self.who_loses()) {
            None => Err(Error::ReceiverNotFound)?,
            Some(user) => user
        };

        let store = match schema.store(&self.store_name().to_owned()) {
            None => Err(Error::StoreDoesNotExist)?,
            Some(store) => store
        };

        if store.owner() != self.who_removes() {
            Err(Error::UserCannotManagePermission)?
        }

        if !who_loses_bonuses.contains_bonus_for_producing(self.store_name()) {
            Err(Error::UserCannotProduceBonus)?
        }

        let new_user = who_loses_bonuses
            .remove_permission_to_producing_bonuses(self.store_name());
        schema.users_mut().put(self.who_loses(), new_user);

        warn!("Remove permission to producing {} from {:?}", self.store_name(), self.who_loses());

        Ok(())
    }
}

impl Transaction for TxAddConversion {
    fn verify(&self) -> bool {
        self.verify_signature(self.first_side_owner())
    }

    fn execute(&self, fork: &mut Fork) -> Result<(), ExecutionError> {
        let mut schema = QiwiloyaltySchema::new(fork);

        let first_side_owner = schema
            .user(&self.first_side_owner().to_owned())
            .ok_or(Error::UserNotExist)?;

        let first_side = schema
            .store(&self.first_side().to_owned())
            .ok_or(Error::StoreDoesNotExist)?;

        let second_side = schema
            .store(&self.second_side().to_owned())
            .ok_or(Error::StoreDoesNotExist)?;

        if self.coefficient().denominator() == 0 {
            Err(Error::ZeroDenominator)?
        }

        if first_side.owner() != first_side_owner.pub_key() {
            Err(Error::UserCannotManagePermission)?
        }

        let conversion_list_for_first_side = schema
            .conversions_submissions_list(&first_side.name().to_owned());

        let conversion =
            Conversion::new(first_side.name(), second_side.name(), self.coefficient());

        println!("{:?}", conversion);

        let second_side_list = schema
            .conversions_submissions_list(&second_side.name().to_owned());

        if conversion_list_for_first_side.is_none() ||
            conversion_list_for_first_side.clone().unwrap()
                .get_by_second_side(second_side.name()).is_none()
        {
            schema.conversions_submissions_queue_mut()
                .put(&second_side.name().to_owned(),
                     match second_side_list {
                        Some(list) =>
                            list.add_conversion(conversion.reverse()),
                        None => ConversionList::new(vec![conversion.reverse()])
                    }
                );

            warn!("Added conversion to submission for {} from {}", second_side.name(),
                  first_side.name());

            return Ok(());
        }

        let suggested_conversion = conversion_list_for_first_side
            .clone()
            .unwrap()
            .get_by_second_side(second_side.name())
            .unwrap();

        println!("{:?}", suggested_conversion);

        if suggested_conversion != conversion {
            Err(Error::ConversionsDontEquals)?
        }

        let first_side_list = schema
            .conversion(&first_side.name().to_owned());
        schema.conversions_mut()
            .put(&first_side.name().to_owned(),
            match first_side_list {
                Some(list) =>
                    list.add_conversion(suggested_conversion.clone()),
                None => ConversionList::new(vec![suggested_conversion.clone()])
            });

        let second_side_list = schema
            .conversion(&second_side.name().to_owned());
        schema.conversions_mut()
            .put(&second_side.name().to_owned(),
                 match second_side_list {
                     Some(list) =>
                         list.add_conversion(suggested_conversion.clone().reverse()),
                     None => ConversionList::new(vec![suggested_conversion.clone().reverse()])
                 }
            );

        schema.conversions_submissions_queue_mut()
            .put(&first_side.name().to_owned(),
                 conversion_list_for_first_side
                     .unwrap()
                     .remove_conversions(|ref c| {
                         c.second_side() == second_side.name()
                     })
            );

        Ok(())
    }
}

impl Transaction for TxTakeBonusWithConversion {
    fn verify(&self) -> bool {
        self.verify_signature(self.who_takes())
    }

    fn execute(&self, fork: &mut Fork) -> Result<(), ExecutionError> {
        let mut schema = QiwiloyaltySchema::new(fork);

        let who_takes = schema
            .user(self.who_takes())
            .ok_or(Error::SenderNotFound)?;

        let who_pays = schema
            .user(self.who_pays())
            .ok_or(Error::ReceiverNotFound)?;

        let store = schema
            .store(&self.store_name().to_owned())
            .ok_or(Error::StoreDoesNotExist)?;

        let paying_store = schema
            .store(&self.paying_store_name().to_owned())
            .ok_or(Error::StoreDoesNotExist)?;

        if !who_takes.contains_bonus_for_producing(self.store_name()) {
            Err(Error::UserCannotProduceBonus)?
        }

        let conversions_list = schema
            .conversion(&paying_store.name().to_owned());

        let conversion = conversions_list
            .unwrap_or(ConversionList::new(vec![]))
            .get_by_second_side(store.name())
            .ok_or(Error::CannotPayThisTypeOfBonuses)?;

        let paying_amount = conversion.coefficient().reverse().multiply(self.amount());

        let (new_user, result) = who_pays
            .take_bonus(&paying_store.name().to_owned(), paying_amount);

        if !result {
            Err(Error::UserHasNoEnoughBonuses)?
        } else {
            schema.users_mut().put(self.who_pays(), new_user);
            Ok(())
        }
    }
}