# blockchain

Exonum blockchain.

address : 40.87.145.4
port : 8000

# Transactions
##### Create user. Signed by pub_key
v1/users/
```json
{
  "body": {
    "pub_key": "ab06539a5e8fdae7207e5eaeda15f339c5dcc8e12a7c6a5b118e7cc58901671b"
  },
  "protocol_version": 0,
  "message_id": 0,
  "service_id": 666,
  "signature": "9d471c86e81690492484857ccc022eee41ffb39bd5f5d7fc1b53627040704d6de21cfe85fe0b6f23e78b6d87072d52dab089569ee07e2e7c075243d77f79180a"
}
```

```java
public class TxCreateUser extends Transaction {
    @SerializedName("pub_key")
    @OrderId(id = 0)
    private CryptoPublicKey publicKey;
    
    public TxCreateUser(CryptoPublicKey publicKey) {
        super((byte) 0, (byte) 0, (short) 0, (short) 666);
        this.publicKey = publicKey;
    }
}
```
##### Create Store. Signed by owner
v1/stores/
```json
{
  "body": {
    "owner": "ece9e889f97748ad1f018ae3fa6cb62bb57c8526f5d9fec64527361fc610c7ab",
    "name": "Magnit"
  },
  "protocol_version": 0,
  "message_id": 3,
  "service_id": 666,
  "signature": "0bc342a6b77a9eb4946c750b35558fecffb0f1718805df3ce46db817a2374ce5c80474410690362c9af5d154ab3a687939d5e76cb979bbabf491c9690f53920e"
}
```
```java
public class TxCreateStore extends Transaction {
    @OrderId(id = 1)
    private CryptoPublicKey owner;

    @OrderId(id = 2)
    private String name;

    public TxCreateStore(CryptoPublicKey owner, String name) {
        super((byte) 0, (byte) 0, (short) 3, (short) 666);
        this.owner = owner;
        this.name = name;
    }
}
```
##### Add permission to producing bonuses. Signed from who_gives
v1/users/addpermission
```json
{
  "body": {
    "who_gives": "ece9e889f97748ad1f018ae3fa6cb62bb57c8526f5d9fec64527361fc610c7ab",
    "who_gets": "ece9e889f97748ad1f018ae3fa6cb62bb57c8526f5d9fec64527361fc610c7ab",
    "store_name": "Magnit"
  },
  "protocol_version": 0,
  "message_id": 4,
  "service_id": 666,
  "signature": "56f33ab38d2ec363c3cffb49f097064bdab002c75a1a1a8817377409208e43c39531c29daade0278c5d066df1c760021d4e4c92daec165afef8d11c42bc42505"
}
```
```java
public class TxAddPermission extends Transaction {
    @SerializedName("who_gives")
    @OrderId(id = 0)
    private CryptoPublicKey whoGives;

    @SerializedName("who_gets")
    @OrderId(id = 1)
    private CryptoPublicKey whoGets;

    @SerializedName("store_name")
    @OrderId(id = 3)
    private String storeName;

    public TxAddPermission(CryptoPublicKey whoGives, CryptoPublicKey whoGets, String storeName) {
        super((byte) 0, (byte) 0, (short) 4, (short) 666);
        this.whoGives = whoGives;
        this.whoGets = whoGets;
        this.storeName = storeName;
    }
}
```
##### Add bonus. Signed by from
v1/users/addbonus
```json
{
  "body": {
    "from": "ece9e889f97748ad1f018ae3fa6cb62bb57c8526f5d9fec64527361fc610c7ab",
    "to": "ab06539a5e8fdae7207e5eaeda15f339c5dcc8e12a7c6a5b118e7cc58901671b",
    "amount": "10",
    "name": "Magnit",
    "combustion": "1000"
  },
  "protocol_version": 0,
  "message_id": 1,
  "service_id": 666,
  "signature": "6e42c6c47707dd6e663014ec8e9b5906a4d99e742147828d80c664b37c991aba8830c3b64ceff078f9910dbf5e95d5e29d7a58e8141f3a8f6994fec19389e80c"
}
```
```java
public class TxAddBonus extends Transaction {
    @OrderId(id = 0)
    private CryptoPublicKey from;

    @OrderId(id = 1)
    private CryptoPublicKey to;

    @OrderId(id = 2)
    private long amount;

    @OrderId(id = 3)
    private String name;

    @OrderId(id = 4)
    private long combustion;

    public TxAddBonus(CryptoPublicKey from, CryptoPublicKey to, long amount, String name, long combustion) {
        super((byte) 0, (byte) 0, (short) 1, (short) 666);
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.name = name;
        this.combustion = combustion;
    }
}
```
##### Remove permission to producing bonuses. Signed by who_removes
```json
{
  "body": {
    "who_removes": "ece9e889f97748ad1f018ae3fa6cb62bb57c8526f5d9fec64527361fc610c7ab",
    "who_loses": "ab06539a5e8fdae7207e5eaeda15f339c5dcc8e12a7c6a5b118e7cc58901671b",
    "store_name": "Magnit"
  },
  "protocol_version": 0,
  "message_id": 5,
  "service_id": 666,
  "signature": "6e42c6c47707dd6e663014ec8e9b5906a4d99e742147828d80c664b37c991aba8830c3b64ceff078f9910dbf5e95d5e29d7a58e8141f3a8f6994fec19389e80c"
} 
```
```java
public class TxRemovePermission extends Transaction {
    @OrderId(id = 0)
    @SerializedName("who_removes")
    private CryptoPublicKey whoRemoves;

    @OrderId(id = 1)
    @SerializedName("who_loses")
    private CryptoPublicKey whoLoses;

    @OrderId(id = 2)
    @SerializedName("store_name")
    private String storeName;
    
    public TxRemovePermission(CryptoPublicKey whoRemoves, CryptoPublicKey whoLoses, String storeName) {
        super((byte) 0, (byte) 0, (short) 5, (short) 666);
        this.whoRemoves = whoRemoves;
        this.whoLoses = whoLoses;
        this.storeName = storeName;
    }
}
```

##### Taking Bonuses. Signed by who_takes
```json
{
  "body": {
    "who_takes": "1d24b7d1e9718e292d311068fb24d03d093c5ac2bc639e3912ec7ef9f54f9d91",
    "who": "639fbc4944d6d3c3783dfdb76f9050f6a2f066ac8c44b8c4ca716714f31eaa36",
    "name": "Magnit",
    "amount": "10",
    "seed": "1000"
  },
  "protocol_version": 0,
  "message_id": 2,
  "service_id": 666,
  "signature": "9126a84733f3b57bf6c7b1647be4956cc7faeb13a40c6529f0185479b7c55ba427f325b26d159aa112e8243f1afe905dd16ce23d44a1db76546fee81b875c108"
}
```
```java
public class TxTakeBonus extends Transaction {
    @OrderId(id = 0)
    @SerializedName("who_takes")
    private CryptoPublicKey whoTakes;

    @OrderId(id = 1)
    private CryptoPublicKey who;

    @OrderId(id = 2)
    private String name;

    @OrderId(id = 3)
    private long amount;

    @OrderId(id = 4)
    private long seed;


    public TxTakeBonus(CryptoPublicKey whoTakes, CryptoPublicKey who, String name, long amount, long seed) {
        super((byte) 0, (byte) 0, (short) 2, (short) 666);
        this.whoTakes = whoTakes;
        this.who = who;
        this.name = name;
        this.amount = amount;
        this.seed = seed;
    }
}
```

##### Add conversion. Signed by first_side_owner
```json
{
    "body": {
        "first_side_owner": "c64b8505041b97f5ddae964eb97d9514723adde8aa7556dd76b1e202d3e6ed5d",
        "first_side": "A",
        "second_side": "B",
        "coefficient": {
            "numerator": "1",
            "denominator": "10"
        }
    },
    "protocol_version": 0,
    "message_id": 6,
    "service_id": 666,
    "signature": "11b2c2873546a4ea0b6152af3b62ed1884edd33a011d6c05557a92bacc1e078b0baec61b7c717566b58502cc0762fdb8ccca5444dde222ec1be1b6d5a0879f06"
}
```
```java
public class TxAddConversion extends Transaction {
    @OrderId(id = 0)
    @SerializedName("first_side_owner")
    private CryptoPublicKey firstSideOwner;

    @OrderId(id = 1)
    @SerializedName("first_side")
    private String firstSide;

    @SerializedName("second_side")
    @OrderId(id = 2)
    private String secondSide;

    @OrderId(id = 3)
    private Coefficients coefficient;

    public TxAddConversion(CryptoPublicKey firstSideOwner, String firstSide, String secondSide, Coefficients coefficient) {
        super((byte) 0, (byte) 0, (short) 6, (short) 666);
        this.firstSideOwner = firstSideOwner;
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.coefficient = coefficient;
    }


    @ExonumStruct
    public static class Coefficients {
        @OrderId(id = 0)
        long numerator;

        @OrderId(id = 1)
        long denominator;

        public Coefficients(long numerator, long denominator) {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }
}
```

##### Take Bonus With Conversion. Signed by who_takes
```json
{
    "body": {
        "who_takes": "572f3542668209541a67591dc25793259a64f76a66b5ac7fdb964464da8e3c80",
        "who_pays": "c64b8505041b97f5ddae964eb97d9514723adde8aa7556dd76b1e202d3e6ed5d",
        "store_name": "B",
        "paying_store_name": "A",
        "amount": "100",
        "seed": "10000"
    },
    "protocol_version": 0,
    "message_id": 7,
    "service_id": 666,
    "signature": "09f4c1cf3693f67abd42917859f6c058e597dee15fc8c50f20f8e6a420f4e3c4d8a24f1e472a99a7a6e6b040f406c9fc96f36e19243e37313f504948f265740b"
}
```

```java
public class TxTakeBonusWithConversion extends Transaction {
    @OrderId(id = 0)
    @SerializedName("who_takes")
    private CryptoPublicKey whoTakes;

    @OrderId(id = 1)
    @SerializedName("who_pays")
    private CryptoPublicKey whoPays;

    @OrderId(id = 3)
    @SerializedName("store_name")
    private String storeName;

    @OrderId(id = 4)
    @SerializedName("paying_store_name")
    private String payingStoreName;

    @OrderId(id = 5)
    private long amount;

    @OrderId(id = 6)
    private long seed;

    public TxTakeBonusWithConversion(CryptoPublicKey whoTakes, CryptoPublicKey whoPays, String storeName,
                                     String payingStoreName, long amount, long seed) {
        super((byte) 0, (byte) 0, (short) 7, (short) 666);
        this.whoTakes = whoTakes;
        this.whoPays = whoPays;
        this.storeName = storeName;
        this.payingStoreName = payingStoreName;
        this.amount = amount;
        this.seed = seed;
    }
}
```

__seed__ necessary for sending same transactions(with different seed)
### Committing
For rebuilding configuration on server mark commit with __";CMD: REBUILD;"__ in commit 
description without quotes.

### Java Library Documentation
[Documentation](https://qiwi-loyalty-2.0.gitlab.io/java-exonum-client)

### Explorer
__{{node_ip}}:{{node_port}}/api/explorer/v1/transactions?hash=__