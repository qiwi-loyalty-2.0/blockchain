use exonum::{
    crypto::PublicKey,
    storage::{Fork, MapIndex, Snapshot}
};

encoding_struct! {
    struct User {
        pub_key: &PublicKey,
        bonuses: Vec<Bonus>,
        /// represents, which bonuses(their names) user can produce
        bonuses_which_can_produce: Vec<BonusInfo>,
    }
}

encoding_struct! {
    struct BonusInfo {
        name: &str
    }
}

encoding_struct! {
    struct Bonus {
        owner : &PublicKey,
        amount : u64,
        store_name : &str,
        combustion : u64,
    }
}

encoding_struct! {
    struct Store {
        owner : &PublicKey,
        name : &str,
        blocked : bool,
    }
}

encoding_struct! {
    struct Conversion {
        first_side: &str,
        second_side: &str,
        /// first_side / coefficient = second_side
        coefficient: Coefficients,
    }
}

encoding_struct! {
    struct Coefficients {
        numerator: u64,
        denominator: u64,
    }
}

encoding_struct! {
    struct ConversionList {
        conversions: Vec<Conversion>,
    }
}

impl Store {
    pub fn block(self) -> Self {
        Self::new(self.owner(), self.name(), true)
    }

    pub fn unblock(self) -> Self {
        Self::new(self.owner(), self.name(), false)
    }
}

impl User {
    pub fn add_bonus(&self, bonus: Bonus) -> Self {
        let mut bonuses = self.bonuses().clone();
        bonuses.push(bonus);
        Self::new(self.pub_key(), bonuses, self.bonuses_which_can_produce())
    }

    pub fn take_bonus(self, name: &String, need: u64) -> (Self, bool) {
        let mut bonuses: Vec<Bonus> = vec![];
        let mut left = need;
        for bonus in self.bonuses().iter() {
            if bonus.store_name() == name && left != 0 {
                if bonus.amount() > left {
                    bonuses.push(Bonus::new(bonus.owner(), bonus.amount() - left,
                                                  bonus.store_name(), bonus.combustion()));
                    left = 0;
                } else {
                    left -= bonus.amount();
                }
            } else {
                bonuses.push(bonus.clone());
            }
        }

        if left == 0 {
            (Self::new(self.pub_key(), bonuses, self.bonuses_which_can_produce()), true)
        } else {
            (self, false)
        }
    }

    pub fn add_permission_to_producing_bonuses(&self, name: &str) -> Self {
        let mut new_permission = self.bonuses_which_can_produce().clone();
        new_permission.push(BonusInfo::new(name));

        Self::new(self.pub_key(), self.bonuses(), new_permission)
    }

    pub fn remove_permission_to_producing_bonuses(&self, name: &str) -> Self {
        let mut new_permission = self.bonuses_which_can_produce().clone();
        new_permission.retain(|ref b| { b.name() != name });

        Self::new(self.pub_key(), self.bonuses(), new_permission)
    }

    pub fn contains_bonus_for_producing(&self, name: &str) -> bool {
        self.bonuses_which_can_produce().iter()
            .find(|&b| { b.name() == name })
            .is_some()
    }
}

impl Coefficients {
    pub fn reverse(&self) -> Self {
        Coefficients::new(self.denominator(), self.numerator())
    }

    pub fn multiply(&self, n: u64) -> u64 {
        n / self.numerator() * self.denominator()
    }
}

impl Conversion {
    pub fn reverse(&self) -> Self {
        Self::new(self.second_side(), self.first_side(), self.coefficient().reverse())
    }
}

impl ConversionList {
    pub fn add_conversion(&self, conversion: Conversion) -> Self {
        let mut new_conversions = self.conversions().clone();
        new_conversions.push(conversion);

        Self::new(new_conversions)
    }

    pub fn get_by_second_side(&self, second_side: &str) -> Option<Conversion> {
        self.conversions().iter()
            .position(|ref conversion| { conversion.second_side() == second_side })
            .map(|index| { self.conversions()[index].clone() })
    }

    pub fn remove_conversions<T>(&self, predicate: T) -> Self
        where T : Fn(&Conversion) -> bool
    {
        let mut new_conversions = self.conversions().clone();
        new_conversions.retain(|ref conversion| { !predicate(&conversion) });
        Self::new(new_conversions)
    }

    pub fn empty() -> Self {
        ConversionList::new(vec![])
    }
}

pub struct QiwiloyaltySchema<T> {
    view: T,
}

impl<T: AsRef<Snapshot>> QiwiloyaltySchema<T> {
    pub fn new(view: T) -> Self {
        QiwiloyaltySchema { view }
    }

    pub fn users(&self) -> MapIndex<&Snapshot, PublicKey, User> {
        MapIndex::new("qiwiloyalty.users", self.view.as_ref())
    }

    // Utility method to quickly get a separate user from the storage
    pub fn user(&self, pub_key: &PublicKey) -> Option<User> {
        self.users().get(pub_key)
    }

    pub fn stores(&self) -> MapIndex<&Snapshot, String, Store> {
        MapIndex::new("qiwiloyalty.stores", self.view.as_ref())
    }

    pub fn store(&self, name: &String) -> Option<Store> {
        self.stores().get(name)
    }

    pub fn conversions(&self) -> MapIndex<&Snapshot, String, ConversionList> {
        MapIndex::new("qiwiloyalty.conversions", self.view.as_ref())
    }

    pub fn conversion(&self, store_name: &String) -> Option<ConversionList> {
        self.conversions().get(store_name)
    }

    /// Map Side, which have to apply -> List of suggested conversions(reverted)
    pub fn conversions_submissions_queue(&self) -> MapIndex<&Snapshot, String, ConversionList> {
        MapIndex::new("qiwiloyalty.conversions_queue", self.view.as_ref())
    }

    pub fn conversions_submissions_list(&self, second_side: &String) -> Option<ConversionList> {
        self.conversions_submissions_queue().get(second_side)
    }
}

impl<'a> QiwiloyaltySchema<&'a mut Fork> {
    pub fn users_mut(&mut self) -> MapIndex<&mut Fork, PublicKey, User> {
        MapIndex::new("qiwiloyalty.users", &mut self.view)
    }

    pub fn stores_mut(&mut self) -> MapIndex<&mut Fork, String, Store> {
        MapIndex::new("qiwiloyalty.stores", &mut self.view)
    }

    pub fn conversions_mut(&mut self) -> MapIndex<&mut Fork, String, ConversionList> {
        MapIndex::new("qiwiloyalty.conversions", &mut self.view)
    }

    pub fn conversions_submissions_queue_mut(&mut self)
        -> MapIndex<&mut Fork, String, ConversionList>
    {
        MapIndex::new("qiwiloyalty.conversions_queue", &mut self.view)
    }
}