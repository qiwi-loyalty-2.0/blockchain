#! /bin/bash

docker build --tag qiwiloyalty_img . 2> buildlog.log > buildlog.log

build_res=$?
if [[ ${build_res} -ne 0 ]]
then
    echo "Build failed"
    exit 1
fi

# docker run -p 8000:8000 --name qiwiloyalty qiwiloyalty_img  & disown 2> log.txt
docker-compose up -d 2> log.log

execution_res=$?

if [[ ${execution_res} -ne 0 ]]
then 
    echo "run failed"
    exit 1
fi

echo "Started!"

docker ps
