#!/usr/bin/env bash

node_count=4
start_peer_port=6331
start_public_port=8000
path_to_app=$1

./init_node.sh ${path_to_app}

cd node_cfg

for i in $(seq 0 $((node_count - 1)))
do
  public_port=$((start_public_port + i))
  private_port=$((public_port + node_count))
  RUST_LOG=warn ${path_to_app} run --node-config node_$((i + 1))_cfg.toml --db-path db$((i + 1)) --public-api-address 0.0.0.0:${public_port} 2>> log$((i + 1)).log & disown
  start_result=$?
  if [[ ${start_result} -ne 0 ]]
  then
    exit 1
  fi
  echo "new node with ports: $public_port (public) and $private_port (private)"
  sleep 1
  ps aux
done

if [[ $2 == "docker" ]]
then
    echo "waiting"

    sleep infinity
fi

