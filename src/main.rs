extern crate exonum;
extern crate qiwiloyalty;
extern crate exonum_configuration;
#[macro_use]
extern crate log;

use exonum::helpers::fabric::NodeBuilder;
use exonum_configuration as configuration;
use qiwiloyalty::ServiceFactory;

fn main() {
    exonum::crypto::init();
    exonum::helpers::init_logger().unwrap();

    let node = NodeBuilder::new()
        .with_service(Box::new(configuration::ServiceFactory))
        .with_service(Box::new(ServiceFactory));

    warn!("Blockchain is running!");

    node.run();
}