#! /bin/bash

echo "start waiting"
for i in {1..10}; do 
	printf "."
	sleep 2
done
echo ""
echo "check connection"

# 5 minutes for waiting
curl_res=`curl ${1} --connect-timeout 300`
res=$?

if [[ $curl_res = "" ]]; then
	echo "connection failed!"
	exit 1
elif [[ $res -ne 0 ]]; then
	echo "bad request!"
	exit 2
fi

echo "connection works!"
exit 0
