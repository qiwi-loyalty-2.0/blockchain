extern crate exonum;
extern crate qiwiloyalty as qiwiloyalty;
extern crate exonum_testkit;

use qiwiloyalty::{
    QiwiloyaltyService,
    schema::{
        QiwiloyaltySchema,
        ConversionList,
        Coefficients,
        Conversion
    },
};

use exonum_testkit::{TestKit, TestKitBuilder};

fn init_testkit() -> TestKit {
    TestKitBuilder::validator()
        .with_service(QiwiloyaltyService)
        .create()
}

#[test]
fn test_conversion_list_add() {
    let list = ConversionList::new(vec![
        Conversion::new("aa", "bb", Coefficients::new(1, 10)),
    ]);

    let list = list.add_conversion(Conversion::new("cc", "dd", Coefficients::new(10, 1)));

    assert_eq!(2, list.conversions().len());
    assert_eq!("cc", list.conversions()[1].first_side());
    assert_eq!("bb", list.conversions()[0].second_side());
}

#[test]
fn test_conversions_schema() {
    let mut testkit = init_testkit();

    let mut fork = testkit.blockchain_mut().fork();

    {
        let mut schema = QiwiloyaltySchema::new(&mut fork);

        schema.conversions_mut().put(&"magnit".to_owned(), ConversionList::new(vec![
            Conversion::new("magnit", "lenta", Coefficients::new(10, 1))
        ]));
    }

    testkit.blockchain_mut().merge(fork.into_patch()).expect("Cannot merge!");

    assert_eq!(1,
               QiwiloyaltySchema::new(&testkit.snapshot())
                   .conversion(&"magnit".to_owned())
                   .expect("Nothing conversion!")
                   .conversions()
                   .len()
    );
}

#[test]
fn test_conversions_submission_queue_schema() {
    let mut testkit = init_testkit();

    let mut fork = testkit.blockchain_mut().fork();

    {
        let mut schema = QiwiloyaltySchema::new(&mut fork);

        schema.conversions_submissions_queue_mut().put(&"magnit".to_owned(),
                                                       ConversionList::new(vec![
            Conversion::new("lenta", "magnit", Coefficients::new(1, 10))
        ]));
    }

    testkit.blockchain_mut().merge(fork.into_patch()).expect("Cannot merge!");

    assert_eq!(1,
               QiwiloyaltySchema::new(&testkit.snapshot())
                   .conversions_submissions_queue()
                   .get(&"magnit".to_owned())
                   .expect("Nothing conversion!")
                   .conversions()
                   .len()
    );
}

