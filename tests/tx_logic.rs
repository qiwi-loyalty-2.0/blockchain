extern crate exonum;
extern crate qiwiloyalty as qiwiloyalty;
#[macro_use] extern crate exonum_testkit;

use exonum::{
    crypto::{self},
    blockchain::TransactionErrorType::Code
};
use exonum_testkit::{TestKit, TestKitBuilder};
use qiwiloyalty::{
    QiwiloyaltyService,
    schema::{
        QiwiloyaltySchema,
        Coefficients,
    },
    transactions::{
        TxCreateUser,
        TxCreateStore,
        TxAddBonus,
        TxTakeBonus,
        TxAddPermissionToProducingBonuses,
        TxRemovePermissionToProducingBonuses,
        Error,
        TxAddConversion,
        TxTakeBonusWithConversion,
    }
};

fn init_testkit() -> TestKit {
    TestKitBuilder::validator()
        .with_service(QiwiloyaltyService)
        .create()
}

#[test]
fn test_create_user() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
    ]);
    let user = {
        let snapshot = testkit.snapshot();
        QiwiloyaltySchema::new(&snapshot).user(&pubkey).expect(
            "No wallet persisted",
        )
    };
    assert_eq!(*user.pub_key(), pubkey);
}

#[test]
fn test_take_bonus() {
    let mut testkit = init_testkit();
    let (alice_pubkey, alice_key) = crypto::gen_keypair();
    let (sender_pubkey, sender_key) = crypto::gen_keypair();

    let store_manager = crypto::gen_keypair();
    let store_name_a = "a".to_owned();
    let store_name_b = "b".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&store_manager.0, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_a, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_b, &store_manager.1),

        TxCreateUser::new(&alice_pubkey, &alice_key),
        TxCreateUser::new(&sender_pubkey, &sender_key),

        TxAddPermissionToProducingBonuses::new(&store_manager.0, &sender_pubkey, &store_name_a, &store_manager.1),
        TxAddPermissionToProducingBonuses::new(&store_manager.0, &sender_pubkey, &store_name_b, &store_manager.1),

        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 12, "a", 0, &sender_key),
        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 24, "b", 0, &sender_key),
        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 12, "a", 0, &sender_key),
    ]);

    for tx in tx_info.iter() {
        println!("{:?}", tx.status());
        assert_eq!(true, tx.status().is_ok());
    }


    testkit.create_block_with_transactions(txvec![
        TxTakeBonus::new(&sender_pubkey, &alice_pubkey, "b", 20, 10, &sender_key),
        TxTakeBonus::new(&sender_pubkey, &alice_pubkey, "a", 23, 11, &sender_key),
    ]);

    let alice = {
        let snapshot = testkit.snapshot();
        QiwiloyaltySchema::new(&snapshot).user(&alice_pubkey).expect(
            "No wallet persisted",
        )
    };

    assert_eq!(alice.bonuses().len(), 2);
    assert_eq!(alice.bonuses()[0].store_name(), "b");
    assert_eq!(alice.bonuses()[0].amount(), 4);
    assert_eq!(alice.bonuses()[1].store_name(), "a");
    assert_eq!(alice.bonuses()[1].amount(), 1);
}

#[test]
fn test_take_bonus_without_permission() {
    let mut testkit = init_testkit();
    let (alice_pubkey, alice_key) = crypto::gen_keypair();
    let (sender_pubkey, sender_key) = crypto::gen_keypair();

    let store_manager = crypto::gen_keypair();
    let store_name_a = "a".to_owned();
    let store_name_b = "b".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&store_manager.0, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_a, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_b, &store_manager.1),

        TxCreateUser::new(&alice_pubkey, &alice_key),
        TxCreateUser::new(&sender_pubkey, &sender_key),
    ]);

    for tx in tx_info.iter() {
        println!("{:?}", tx.status());
        assert_eq!(true, tx.status().is_ok());
    }

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxTakeBonus::new(&sender_pubkey, &alice_pubkey, "b", 20, 10, &sender_key),
        TxTakeBonus::new(&sender_pubkey, &alice_pubkey, "a", 23, 11, &sender_key),
    ]);

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                code == Error::UserCannotProduceBonus as u8
            } else { false }
            _ => false
        });
    }
}

#[test]
fn test_add_bonus() {
    let mut testkit = init_testkit();
    let (alice_pubkey, alice_key) = crypto::gen_keypair();
    let (sender_pubkey, sender_key) = crypto::gen_keypair();

    let store_manager = crypto::gen_keypair();
    let store_name_a = "a".to_owned();
    let store_name_b = "b".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&store_manager.0, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_a, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_b, &store_manager.1),

        TxCreateUser::new(&sender_pubkey, &sender_key),
        TxCreateUser::new(&alice_pubkey, &alice_key),

        TxAddPermissionToProducingBonuses::new(&store_manager.0, &sender_pubkey, &store_name_a, &store_manager.1),
        TxAddPermissionToProducingBonuses::new(&store_manager.0, &sender_pubkey, &store_name_b, &store_manager.1),

        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 12, "a", 0, &sender_key),
        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 24, "b", 0, &sender_key),
        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 12, "a", 0, &sender_key),
    ]);

    for tx in tx_info.iter() {
        println!("{:?}", tx.status());
        assert_eq!(true, tx.status().is_ok());
    }

    let alice = {
        let snapshot = testkit.snapshot();
        QiwiloyaltySchema::new(&snapshot).user(&alice_pubkey).expect(
            "No wallet persisted",
        )
    };

    assert_eq!(alice.bonuses().len(), 3);

    // assert_eq!(alice.bonuses()[0], test_bonus);
    // assert_eq!(alice.bonuses()[0].amount(), test_bonus.amount());
    // assert_eq!(alice.bonuses()[0].name(), test_bonus.name());

    // assert_eq!(alice.bonuses()[1], test_bonus2);
    // assert_eq!(alice.bonuses()[1].amount(), test_bonus2.amount());
    // assert_eq!(alice.bonuses()[1].name(), test_bonus2.name());
}

#[test]
fn test_add_wrong_bonuses() {
    let mut testkit = init_testkit();
    let (alice_pubkey, alice_key) = crypto::gen_keypair();
    let (sender_pubkey, sender_key) = crypto::gen_keypair();
    let store_manager = crypto::gen_keypair();
    let store_name_a = "a".to_owned();
    let store_name_b = "b".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&store_manager.0, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_a, &store_manager.1),
        TxCreateStore::new(&store_manager.0, &store_name_b, &store_manager.1),

        TxCreateUser::new(&sender_pubkey, &sender_key),
        TxCreateUser::new(&alice_pubkey, &alice_key),

        TxAddPermissionToProducingBonuses::new(&store_manager.0, &sender_pubkey, &store_name_a,
                                                &store_manager.1),
        TxAddPermissionToProducingBonuses::new(&store_manager.0, &sender_pubkey, &store_name_b,
                                                &store_manager.1),

        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 12, "a", 0, &sender_key),
        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 24, "b", 0, &sender_key),
    ]);

    for tx in tx_info.iter() {
        println!("{:?}", tx.status());
        assert_eq!(true, tx.status().is_ok());
    }

    let tx_info = testkit.create_block_with_transaction(
        TxAddBonus::new(&sender_pubkey, &alice_pubkey, 10, "c", 0, &sender_key)
    );

    for tx in tx_info.iter() {
        println!("{:?}", tx.status());
        assert_eq!(false, tx.status().is_ok());
    }
}

#[test]
fn test_create_store() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
        TxCreateStore::new(&pubkey, &store_name, &key),
    ]);

    let store = {
        let snapshot = testkit.snapshot();
        QiwiloyaltySchema::new(&snapshot).store(&store_name).expect(
            "No stores persisted",
        )
    };

    assert_eq!(&store_name, store.name());
    assert_eq!(pubkey, *store.owner());
}

#[test]
fn test_shop_with_same_names() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateStore::new(&pubkey, &store_name, &key),
    ]);

    // check, that store created
    for tx in tx_info.iter() {
        assert_eq!(true, tx.status().is_ok());
    }

    let tx_info =
        testkit.create_block_with_transaction(TxCreateStore::new(&pubkey1, &store_name, &key1));

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                code == Error::StoreAlreadyExists as u8
            } else { false }
            _ => false
        });
    }
}

#[test]
fn test_create_store_russian_letters() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let store_name = "Сашина лавка".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
        TxCreateStore::new(&pubkey, &store_name, &key),
    ]);

    let store = {
        let snapshot = testkit.snapshot();
        QiwiloyaltySchema::new(&snapshot).store(&store_name).expect(
            "No stores persisted",
        )
    };

    assert_eq!(&store_name, store.name());
    assert_eq!(pubkey, *store.owner());
}

#[test]
fn test_add_permission() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
        TxCreateUser::new(&pubkey1, &key1),

        TxCreateStore::new(&pubkey1, &store_name, &key1),
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1),
    ]);

    for tx in tx_info.iter() {
        assert_eq!(true, tx.status().is_ok());
    }

    let user = {
        let snapshot = testkit.snapshot();
        QiwiloyaltySchema::new(snapshot).user(&pubkey).expect("No users!")
    };

    assert!(user.contains_bonus_for_producing(&store_name));
}

#[test]
fn test_wrong_permission_adding_signature() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
        TxCreateUser::new(&pubkey1, &key1),

        TxCreateStore::new(&pubkey1, &store_name, &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key)
    );

    for tx in tx_info.iter() {
        assert_eq!(false, tx.status().is_ok());
    }
}

#[test]
fn test_wrong_permission_unknown_who_gives() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type()
                                        { code == Error::UserNotExist as u8}
                                        else { false },
            Ok(()) => false
        });
    }
}

#[test]
fn test_wrong_permission_unknown_receiver() {
    let mut testkit = init_testkit();
    let (pubkey, _key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type()
            { code == Error::ReceiverNotFound as u8}
            else { false },
            Ok(()) => false
        });
    }
}

#[test]
fn test_wrong_permission_unknown_store() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey, &key),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type()
            { code == Error::StoreDoesNotExist as u8}
            else { false },
            Ok(()) => false
        });
    }
}

#[test]
fn test_remove_permission_success() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey, &key),

        TxCreateStore::new(&pubkey1, &store_name, &key1),
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxRemovePermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Ok(()) => true,
            _ => false
        });
    }
}

#[test]
fn test_wrong_remove_permission_signature() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey, &key),

        TxCreateStore::new(&pubkey1, &store_name, &key1),
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxRemovePermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key)
    );

    for tx in tx_info.iter() {
        assert!(tx.status().is_err());
    }
}

#[test]
fn test_wrong_remove_permission_unknown_who_removes() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxRemovePermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                code == Error::UserNotExist as u8
            } else { false },
            Ok(()) => false
        });
    }
}

#[test]
fn test_wrong_unknown_bonus_name() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey, &key),
        TxCreateUser::new(&pubkey1, &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxRemovePermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                code == Error::StoreDoesNotExist as u8
            } else { false },
            Ok(()) => false
        });
    }
}

#[test]
fn test_wrong_user_cannot_manage_permission() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey, &key),

        TxCreateStore::new(&pubkey1, &store_name, &key1),
        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxRemovePermissionToProducingBonuses::new(&pubkey, &pubkey, &store_name, &key)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                code == Error::UserCannotManagePermission as u8
            } else { false },
            Ok(()) => false
        });
    }
}

#[test]
fn test_wrong_user_has_no_permission_to_producing() {
    let mut testkit = init_testkit();
    let (pubkey, key) = crypto::gen_keypair();
    let (pubkey1, key1) = crypto::gen_keypair();
    let store_name = "Peterochka".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey, &key),

        TxCreateStore::new(&pubkey1, &store_name, &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxRemovePermissionToProducingBonuses::new(&pubkey1, &pubkey, &store_name, &key1)
    );

    for tx in tx_info.iter() {
        assert!(match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                code == Error::UserCannotProduceBonus as u8
            } else { false },
            Ok(()) => false
        });
    }
}

#[test]
fn test_add_conversion() {
    let mut testkit = init_testkit();
    let (pubkey1, key1) = crypto::gen_keypair();
    let (pubkey2, key2) = crypto::gen_keypair();
    let store_name1 = "A".to_owned();
    let store_name2 = "B".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey2, &key2),

        TxCreateStore::new(&pubkey1, &store_name1, &key1),
        TxCreateStore::new(&pubkey2, &store_name2, &key2),

        TxAddConversion::new(&pubkey1, &store_name1, &store_name2, Coefficients::new(1, 10), &key1),
        TxAddConversion::new(&pubkey2, &store_name2, &store_name1, Coefficients::new(10, 1), &key2),
    ]);

    for tx in tx_info.iter() {
        assert!(tx.status().is_ok());
    }

    let list = {
        let schema
            = QiwiloyaltySchema::new(testkit.snapshot());

        schema.conversions_submissions_list(&store_name2).unwrap()
    };

    assert_eq!(0, list.conversions().len());
}

#[test]
fn test_wrong_add_conversion() {
    let mut testkit = init_testkit();
    let (pubkey1, key1) = crypto::gen_keypair();
    let (pubkey2, key2) = crypto::gen_keypair();
    let store_name1 = "A".to_owned();
    let store_name2 = "B".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey2, &key2),

        TxCreateStore::new(&pubkey1, &store_name1, &key1),
        TxCreateStore::new(&pubkey2, &store_name2, &key2),

        TxAddConversion::new(&pubkey1, &store_name1, &store_name2, Coefficients::new(1, 10), &key1),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxAddConversion::new(&pubkey2, &store_name2, &store_name1, Coefficients::new(9, 1), &key2)
    );

    for tx in tx_info.iter() {
        match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                assert_eq!(Error::ConversionsDontEquals as u8, code);
            } else { assert_ne!(2, 2); },
            Ok(()) => assert_ne!(3, 3)
        }
    }

    let list = {
        let schema
            = QiwiloyaltySchema::new(testkit.snapshot());

        schema.conversions_submissions_list(&store_name2).unwrap()
    };

    assert_eq!(1, list.conversions().len());
}

#[test]
fn test_take_bonuses_with_conversion() {
    let mut testkit = init_testkit();
    let (pubkey1, key1) = crypto::gen_keypair();
    let (pubkey2, key2) = crypto::gen_keypair();
    let store_name1 = "A".to_owned();
    let store_name2 = "B".to_owned();

    let tx_info = testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey2, &key2),

        TxCreateStore::new(&pubkey1, &store_name1, &key1),
        TxCreateStore::new(&pubkey2, &store_name2, &key2),

        TxAddConversion::new(&pubkey1, &store_name1, &store_name2, Coefficients::new(1, 10), &key1),
        TxAddConversion::new(&pubkey2, &store_name2, &store_name1, Coefficients::new(10, 1), &key2),

        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey1, &store_name1, &key1),
        TxAddPermissionToProducingBonuses::new(&pubkey2, &pubkey2, &store_name2, &key2),

        TxAddBonus::new(&pubkey1, &pubkey1, 3, &store_name1, 100, &key1),
        TxAddBonus::new(&pubkey1, &pubkey1, 7, &store_name1, 101, &key1),
        TxAddBonus::new(&pubkey2, &pubkey1, 2, &store_name2, 102, &key2),

        TxTakeBonusWithConversion::new(&pubkey2, &pubkey1, &store_name2, &store_name1, 100, 5, &key2)
    ]);

    for tx in tx_info.iter() {
        assert!(tx.status().is_ok());
    }

    let user = {
        let schema = QiwiloyaltySchema::new(testkit.snapshot());
        schema.user(&pubkey1).expect("How???")
    };

    assert_eq!(1, user.bonuses().len());
}

#[test]
fn test_take_bonuses_with_conversion_nothing_conversions() {
    let mut testkit = init_testkit();
    let (pubkey1, key1) = crypto::gen_keypair();
    let (pubkey2, key2) = crypto::gen_keypair();
    let store_name1 = "A".to_owned();
    let store_name2 = "B".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey2, &key2),

        TxCreateStore::new(&pubkey1, &store_name1, &key1),
        TxCreateStore::new(&pubkey2, &store_name2, &key2),

        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey1, &store_name1, &key1),
        TxAddPermissionToProducingBonuses::new(&pubkey2, &pubkey2, &store_name2, &key2),

        TxAddBonus::new(&pubkey1, &pubkey1, 3, &store_name1, 100, &key1),
        TxAddBonus::new(&pubkey1, &pubkey1, 7, &store_name1, 101, &key1),
        TxAddBonus::new(&pubkey2, &pubkey1, 2, &store_name2, 102, &key2),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxTakeBonusWithConversion::new(&pubkey2, &pubkey1, &store_name2, &store_name1, 100, 5, &key2)
    );

    for tx in tx_info.iter() {
        match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                assert_eq!(Error::CannotPayThisTypeOfBonuses as u8, code);
            } else { assert_ne!(2, 2); },
            Ok(()) => assert_ne!(3, 3)
        }
    }

    let user = {
        let schema = QiwiloyaltySchema::new(testkit.snapshot());
        schema.user(&pubkey1).expect("How???")
    };

    assert_eq!(3, user.bonuses().len());
}

#[test]
fn test_take_bonuses_with_conversion_not_enough_bonuses() {
    let mut testkit = init_testkit();
    let (pubkey1, key1) = crypto::gen_keypair();
    let (pubkey2, key2) = crypto::gen_keypair();
    let store_name1 = "A".to_owned();
    let store_name2 = "B".to_owned();

    testkit.create_block_with_transactions(txvec![
        TxCreateUser::new(&pubkey1, &key1),
        TxCreateUser::new(&pubkey2, &key2),

        TxCreateStore::new(&pubkey1, &store_name1, &key1),
        TxCreateStore::new(&pubkey2, &store_name2, &key2),

        TxAddConversion::new(&pubkey1, &store_name1, &store_name2, Coefficients::new(1, 10), &key1),
        TxAddConversion::new(&pubkey2, &store_name2, &store_name1, Coefficients::new(10, 1), &key2),

        TxAddPermissionToProducingBonuses::new(&pubkey1, &pubkey1, &store_name1, &key1),
        TxAddPermissionToProducingBonuses::new(&pubkey2, &pubkey2, &store_name2, &key2),

        TxAddBonus::new(&pubkey1, &pubkey1, 2, &store_name1, 100, &key1),
        TxAddBonus::new(&pubkey1, &pubkey1, 7, &store_name1, 101, &key1),
        TxAddBonus::new(&pubkey2, &pubkey1, 2, &store_name2, 102, &key2),
    ]);

    let tx_info = testkit.create_block_with_transaction(
        TxTakeBonusWithConversion::new(&pubkey2, &pubkey1, &store_name2, &store_name1, 100, 5, &key2)
    );

    for tx in tx_info.iter() {
        match tx.status() {
            Err(e) => if let Code(code) = e.error_type() {
                assert_eq!(Error::UserHasNoEnoughBonuses as u8, code);
            } else { assert_ne!(2, 2); },
            Ok(()) => assert_ne!(3, 3)
        }
    }

    let user = {
        let schema = QiwiloyaltySchema::new(testkit.snapshot());
        schema.user(&pubkey1).expect("How???")
    };

    assert_eq!(3, user.bonuses().len());
}