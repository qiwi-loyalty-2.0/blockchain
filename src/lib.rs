#[macro_use]
extern crate exonum;
#[macro_use]
extern crate failure;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[macro_use]
extern crate log;

use exonum::{
    blockchain::{
        Service, Transaction,
        TransactionSet},
    crypto::{Hash},
    encoding,
    messages::{RawTransaction},
    storage::{Snapshot},
    helpers::fabric::{self, Context},
    api::{ServiceApiBuilder}
};

// use iron::headers::{AccessControlAllowOrigin};

pub mod api;
pub mod schema;
pub mod transactions;

use crate::{
    transactions::QiwiLoyaltyTransactions,
    api::QiwiloyaltyApi
};

// Service identifier
pub const SERVICE_ID: u16 = 666;
pub const SERVICE_NAME: &str = "qiwiloyalty";

pub struct QiwiloyaltyService;

impl Service for QiwiloyaltyService {
    fn service_id(&self) -> u16 { SERVICE_ID }

    fn service_name(&self) -> &'static str { SERVICE_NAME }

    fn state_hash(&self, _: &Snapshot) -> Vec<Hash> {
        vec![]
    }

    fn tx_from_raw(&self, raw: RawTransaction) -> Result<Box<Transaction>, encoding::Error> {
        let tx = QiwiLoyaltyTransactions::tx_from_raw(raw)?;
        Ok(tx.into())
    }

    fn wire_api(&self, builder: &mut ServiceApiBuilder) {
        QiwiloyaltyApi::wire(builder)
    }
}

/// A configuration service creator for the `NodeBuilder`.
#[derive(Debug)]
pub struct ServiceFactory;

impl fabric::ServiceFactory for ServiceFactory {
    fn service_name(&self) -> &str {
        SERVICE_NAME
    }

    fn make_service(&mut self, _: &Context) -> Box<dyn Service> {
        Box::new(QiwiloyaltyService)
    }
}
