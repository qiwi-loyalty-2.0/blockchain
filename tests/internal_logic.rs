extern crate exonum;
extern crate qiwiloyalty as qiwiloyalty;

use exonum::{
    crypto::{self},
};
use qiwiloyalty::{
    schema::{
        User,
        Bonus
    },
};

#[test]
fn test_removing_bonuses_some() {
    let (pubkey, _seckey) = crypto::gen_keypair();
    let store_name = "Magnit".to_owned();
    let user = User::new(&pubkey, vec![
        Bonus::new(&pubkey, 10, &store_name, 10),
        Bonus::new(&pubkey, 2, &store_name, 13)
    ], vec![]);

    let (user, result) = user.take_bonus(&store_name, 3);

    assert!(result);
    assert_eq!(2, user.bonuses().len());
}

#[test]
fn test_removing_bonuses_all() {
    let (pubkey, _seckey) = crypto::gen_keypair();
    let store_name = "Magnit".to_owned();
    let user = User::new(&pubkey, vec![
        Bonus::new(&pubkey, 10, &store_name, 10),
        Bonus::new(&pubkey, 2, &store_name, 13)
    ], vec![]);

    let (user, result) = user.take_bonus(&store_name, 12);

    assert!(result);
    assert_eq!(0, user.bonuses().len());
}

#[test]
fn test_removing_bonuses_from_middle() {
    let (pubkey, _seckey) = crypto::gen_keypair();
    let store_name = "Magnit".to_owned();
    let second_store_name = "Lenta".to_owned();
    let user = User::new(&pubkey, vec![
        Bonus::new(&pubkey, 10, &store_name, 10),
        Bonus::new(&pubkey, 7, &second_store_name, 10),
        Bonus::new(&pubkey, 2, &store_name, 13),
    ], vec![]);

    let (user, result) = user.take_bonus(&second_store_name, 3);

    assert!(result);
    assert_eq!(3, user.bonuses().len());
    assert_eq!(4, user.bonuses()[1].amount());
}

#[test]
fn test_cannot_remove() {
    let (pubkey, _seckey) = crypto::gen_keypair();
    let store_name = "Magnit".to_owned();
    let second_store_name = "Lenta".to_owned();
    let user = User::new(&pubkey, vec![
        Bonus::new(&pubkey, 10, &store_name, 10),
        Bonus::new(&pubkey, 7, &second_store_name, 10),
        Bonus::new(&pubkey, 2, &store_name, 13),
    ], vec![]);

    let (user, result) = user.take_bonus(&second_store_name, 8);

    assert!(!result);
    assert_eq!(3, user.bonuses().len());
    assert_eq!(7, user.bonuses()[1].amount());
}

#[test]
fn test_remove_all_and_leave_one_type() {
    let (pubkey, _seckey) = crypto::gen_keypair();
    let store_name = "Magnit".to_owned();
    let second_store_name = "Lenta".to_owned();
    let user = User::new(&pubkey, vec![
        Bonus::new(&pubkey, 10, &store_name, 10),
        Bonus::new(&pubkey, 7, &second_store_name, 10),
        Bonus::new(&pubkey, 2, &store_name, 13),
    ], vec![]);

    let (user, result) = user.take_bonus(&store_name, 12);

    assert!(result);
    assert_eq!(1, user.bonuses().len());
    assert_eq!("Lenta", user.bonuses()[0].store_name());
    assert_eq!(7, user.bonuses()[0].amount());
}

#[test]
fn test_try_remove_from_empty() {
    let (pubkey, _seckey) = crypto::gen_keypair();
    let store_name = "Magnit".to_owned();

    let user = User::new(&pubkey, vec![], vec![]);

    let (user, result) = user.take_bonus(&store_name, 12);

    assert!(!result);
    assert_eq!(0, user.bonuses().len());
}

#[test]
fn test_try_remove_not_existing() {
    let (pubkey, _seckey) = crypto::gen_keypair();
    let store_name = "Magnit".to_owned();
    let second_store_name = "Lenta".to_owned();

    let user = User::new(&pubkey, vec![
        Bonus::new(&pubkey, 10, &store_name, 10),
        Bonus::new(&pubkey, 2, &store_name, 13),
    ], vec![]);

    let (user, result) = user.take_bonus(&second_store_name, 12);

    assert!(!result);
    assert_eq!(2, user.bonuses().len());
    assert_eq!(store_name, user.bonuses()[0].store_name());
    assert_eq!(store_name, user.bonuses()[1].store_name());
}