FROM rust:1.32 

ENV ROCKSDB_LIB_DIR=/usr/lib/x86_64-linux-gnu 
ENV SNAPPY_LIB_DIR=/usr/lib/x86_64-linux-gnu 

RUN apt-get update \ 
&& apt-get install -y curl git gcc cpp \ 
build-essential libsodium-dev libsnappy-dev \ 
librocksdb-dev pkg-config 

# RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable 

#RUN curl -sL https://deb.nodesource.com/setup_8.x | bash \ 
# && apt-get install -y nodejs 

COPY src/ /usr/blockchain/src
COPY Cargo.toml /usr/blockchain
COPY Cargo.lock /usr/blockchain

WORKDIR /usr/blockchain

# RUN mv /root/.cargo/bin/* /usr/bin

# RUN rustup update

RUN cargo build --release

# useful scripts for running
COPY init_node.sh /usr/blockchain
COPY launch_node.sh /usr/blockchain
COPY commitmsg /usr/blockchain
COPY clean.sh /usr/blockchain

RUN ls /usr/blockchain/

CMD ["./launch_node.sh", "/usr/blockchain/target/release/qiwiloyalty", "docker"]