#! /bin/bash

docker-compose down

stop_res=$?
if [[ ${stop_res} -ne 0 ]]
then
    echo "Stop failed"
    exit 1
fi

echo "OK"