use exonum::{
    blockchain::Transaction,
    crypto::{Hash, PublicKey},
    node::TransactionSend,
    api::{ServiceApiState, ServiceApiBuilder, self}
};

use crate::{
    transactions::QiwiLoyaltyTransactions,
    schema::{
        QiwiloyaltySchema,
        User,
        Store,
        ConversionList
    }
};

pub struct QiwiloyaltyApi;

#[derive(Serialize, Deserialize, Debug)]
pub struct TransactionResponse {
    // Hash of the transaction.
    pub tx_hash: Hash,
}

impl QiwiloyaltyApi {
    fn post_transaction(state: &ServiceApiState, query: QiwiLoyaltyTransactions)
                        -> api::Result<TransactionResponse> {

        warn!("{:?}", query);

        match query {
            QiwiLoyaltyTransactions::TxCreateUser(_) => println!("kek!"),
            _ => println!("Lol")
        };

        let transaction: Box<Transaction> = query.into();
        let tx_hash = transaction.hash();
        state.sender().send(transaction)?;
        Ok(TransactionResponse { tx_hash })
    }
}

#[derive(Deserialize, Serialize)]
/// The structure describes the query parameters for the `get_user` endpoint.
pub struct UserQuery {
    /// Public key of the queried user.
    pub pub_key: PublicKey,
}

#[derive(Deserialize, Serialize)]
/// The structure describes the query parameters for the `get_store` endpoint.
pub struct StoreQuery {
    /// ID of the queried store.
    pub name: String,
}

impl QiwiloyaltyApi {
    /// Endpoint for getting a single user.
    fn get_user(state: &ServiceApiState, query: UserQuery)
                -> api::Result<User> {
        let snapshot = state.snapshot();
        let schema = QiwiloyaltySchema::new(snapshot);
        schema
            .user(&query.pub_key)
            .ok_or_else(|| api::Error::NotFound("User not found".to_owned()))
    }

    /// Endpoint for dumping all users from the storage.
    fn get_users(state: &ServiceApiState, _query: ())
                 -> api::Result<Vec<User>> {
        let snapshot = state.snapshot();
        let schema = QiwiloyaltySchema::new(snapshot);
        let idx = schema.users();
        let wallets = idx.values().collect();
        Ok(wallets)
    }

    /// Endpoint for getting a single store
    fn get_store(state: &ServiceApiState, query: StoreQuery) -> api::Result<Store> {
        let snapshot = state.snapshot();
        let schema = QiwiloyaltySchema::new(snapshot);
        schema.store(&query.name).ok_or_else(
            || api::Error::NotFound("Store not found".to_owned()))
    }

    /// Endpoint for dumping all stores from the storage.
    fn get_stores(state: &ServiceApiState, _query: ())
                  -> api::Result<Vec<Store>> {
        let snapshot = state.snapshot();
        let schema = QiwiloyaltySchema::new(snapshot);
        let idx = schema.stores();
        let stores = idx.values().collect();
        Ok(stores)
    }

    fn get_conversions(state: &ServiceApiState, query: StoreQuery)
        -> api::Result<ConversionList>
    {
        let snapshot = state.snapshot();
        let schema = QiwiloyaltySchema::new(snapshot);
        Ok(schema.conversion(&query.name).unwrap_or(ConversionList::empty()))
    }
}

impl QiwiloyaltyApi {
    pub fn wire(builder: &mut ServiceApiBuilder) {
        // Binds handlers to specific routes.
        builder
            .public_scope()

            // Read only endpoints uses `GET` method.
            .endpoint("v1/user", Self::get_user)
            .endpoint("v1/users", Self::get_users)
            .endpoint("v1/store", Self::get_store)
            .endpoint("v1/stores", Self::get_stores)
            .endpoint("v1/conversions", Self::get_conversions)
            // But for methods that can modify service state you should use
            // `endpoint_mut` that uses `POST` method.
            .endpoint_mut("v1/users", Self::post_transaction)
            .endpoint_mut("v1/users/addbonus", Self::post_transaction)
            .endpoint_mut("v1/stores", Self::post_transaction)
            .endpoint_mut("v1/users/takebonus", Self::post_transaction)
            .endpoint_mut("v1/users/takebonuswithconversion", Self::post_transaction)
            .endpoint_mut("v1/users/removepermission", Self::post_transaction)
            .endpoint_mut("v1/users/addpermission", Self::post_transaction)
            .endpoint_mut("v1/conversions/addconversion", Self::post_transaction);
    }
}